
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";

  // TODO: Potrebno implementirati

  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function myFunction() {
    var popup = document.getElementById("myPopup");
    popup.classList.toggle("show");
}


function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val(); // pridobimo ime iz polja z ID-jem kreirajIme
	var priimek = $("#kreirajPriimek").val();
	var visina = $("#kreirajVisino").val();
	var teza = $("#kreirajTezo").val();
	var ehrId;

	if (!ime || !priimek || !visina || !teza || ime.trim().length == 0 || priimek.trim().length == 0 || visina.trim().length == 0 || teza.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr", // pošiljanje zahteve
		    type: 'POST',
		    success: function (data) {
		        ehrId = data.ehrId; // ko dobimo odgovor
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            partyAdditionalInfo: [{key: "penis", value: visina}, {key: "weight", value: teza}, {key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party", // da vemo, kakšna je forma, moramo pogledati dokumentacijo na ehr scape
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {  // uspešno
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "'.</span>");
		                    console.log("Uspešno kreiran EHR '" + ehrId + "'.");
		                    $("#preberiEHRid").val(ehrId);
		                    
		                    
		                    var datumInUra = "2014-11-21T11:40Z";
							var telesnaVisina = visina;
							var telesnaTeza = teza;
							var telesnaTemperatura = "36";
							var sistolicniKrvniTlak = "120";
							var diastolicniKrvniTlak = "120";
							var nasicenostKrviSKisikom = "90";
							var merilec = "Trump";
						
							if (!ehrId || ehrId.trim().length == 0) {
								$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
							} else {
								$.ajaxSetup({
								    headers: {"Ehr-Session": sessionId}
								});
								var podatki = { // source predloge za vital signs
									// Preview Structure: https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
								    "ctx/language": "en",
								    "ctx/territory": "SI",
								    "ctx/time": datumInUra,
								    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
								    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
								   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
								    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
								    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
								    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
								    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
								};
								var parametriZahteve = { // klic template-a
								    "ehrId": ehrId,
								    templateId: 'Vital Signs', // template id
								    format: 'FLAT',
								    committer: merilec
								};
								$.ajax({
								    url: baseUrl + "/composition?" + $.param(parametriZahteve),
								    type: 'POST',
								    contentType: 'application/json',
								    data: JSON.stringify(podatki),
								    success: function (res) { // pošiljanje podatkov na server
								    	console.log(res.meta.href);
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'>Uspešno kreiran EHR '" + ehrId + "' + dodani podatki.</span>");
								    },
								    error: function(err) {
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
										console.log(JSON.parse(err.responseText).userMessage);
								    }
								});
							}
		                    
		                }
		            },
		            error: function(err) {  // neuspešno
		            	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
		            	console.log(JSON.parse(err.responseText).userMessage);
		            }
		        });
		    }
		});
	}
}


function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevan podatek!</span>");
	} else {
		
		var teza;
		var visina;
		
		
		var AQL = 
			
		"select " +
		"a_a/data[at0002]/events[at0003]/data[at0001]/items[at0004, 'Body weight']/value/magnitude as BWM " +
		"from EHR e[e/ehr_id/value='" + ehrId + "'] " +
		"contains OBSERVATION a_a[openEHR-EHR-OBSERVATION.body_weight.v1] " +
		"offset 0 limit 1";
				
		$.ajax({
		    url: baseUrl + "/query?" + $.param({"aql": AQL}),
		    type: 'GET',
		    headers: {"Ehr-Session": sessionId},
		    success: function (res) {
		    	
		    	//console.log(res.resultSet[0]);
		    	teza = res.resultSet[0].BWM;
		    	

		    },
		    error: function() {
		    	alert("Napaka v poizvedbi");
		    }
		});
		 
        $.ajax({
            url: baseUrl + "/view/" + ehrId + "/height",
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
            success: function (data) {
                visina = data[0].height;
                //console.log(data);
            }
        });
		
		
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
	    		//console.log(data);
				var party = data.party;
				//console.log(party);
				//console.log(party.partyAdditionalInfo);
				
				//Metric BMI Formula
				//BMI = ( Weight in Kilograms / ( Height in Meters x Height in Meters ) )
				patientBMI = Math.round(teza / (visina/100 * visina/100));
				//$("#preberiSporocilo").html("<span class='obvestilo label label-success fade-in'>Pacient '" + party.firstNames + " " + party.lastNames + "', s tezo " + teza + " , visino " + visina + " in BMI " + patientBMI + ".</span>");
				if(patientBMI > 0)
					$("#pacientov-bmi").html("<br><br>Pacientov BMI je: <b>" + patientBMI + "</b>.");
				if(patientBMI < 19 || patientBMI > 29)
					$("#prehrana-tekst").html("<br><b>Start running fatty</b>");
				else
					$("#prehrana-tekst").html("<br>We choo.");
				if(countryBMI > 0)
					$("#odstotek-predebelih").html("<br><b>Zanimivost!</b> Odstotek odsraslih s preveliko težo v državi " + country + " je " + countryBMI + ".");
				d3.select("svg")
       				.remove();
				narisiGraf(patientBMI);
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
				console.log(JSON.parse(err.responseText).userMessage);
			}
		});
	}	
}

/**
  <div id="diagram-container" class="col-lg-9 col-md-9 col-sm-9">
  				<div class="panel panel-default">
  					<div class="panel-heading">
  						<div class="row">
  							<div class="col-lg-12 col-md-12 col-sm-12"><b>BMI</b> porazdelitev v ZDA</div>
  						</div>
  					</div>
  					<div class="panel-body">
  					  
  					 <img src="http://calorielab.com/news/wp-images/post-images/fattest-states-2015-big.jpg">
  					 <!-- Make America Great Again--> 
  		
    <div id="tooltip"></div><!-- div to hold tooltip. -->
    <svg width="100%" height="100%" id="statesvg"></svg> <!-- svg to hold the map. -->
    <script src="uStates.js"></script> <!-- creates uStates. -->
    <script src="http://d3js.org/d3.v3.min.js"></script>
    <script>
    
    	function tooltipHtml(n, d){	 function to create html content string in tooltip div. 
    		return "<h4>"+n+"</h4><table>"+
    			"<tr><td>Low</td><td>"+(d.low)+"</td></tr>"+
    			"<tr><td>Average</td><td>"+(d.avg)+"</td></tr>"+
    			"<tr><td>High</td><td>"+(d.high)+"</td></tr>"+
    			"</table>";
    	}
    	
    	var sampleData ={};	 Sample random data. 	
    	["HI", "AK", "FL", "SC", "GA", "AL", "NC", "TN", "RI", "CT", "MA",
    	"ME", "NH", "VT", "NY", "NJ", "PA", "DE", "MD", "WV", "KY", "OH", 
    	"MI", "WY", "MT", "ID", "WA", "DC", "TX", "CA", "AZ", "NV", "UT", 
    	"CO", "NM", "OR", "ND", "SD", "NE", "IA", "MS", "IN", "IL", "MN", 
    	"WI", "MO", "AR", "OK", "KS", "LS", "VA"]
    		.forEach(function(d){ 
    			var low=Math.round(100*Math.random()), 
    				mid=Math.round(100*Math.random()), 
    				high=Math.round(100*Math.random());
    			sampleData[d]={low:d3.min([low,mid,high]), high:d3.max([low,mid,high]), 
    					avg:Math.round((low+mid+high)/3), color:d3.interpolate("#ffffcc", "#800026")(low/100)}; 
    		});
    	
    	 draw states on id #statesvg 	
    	uStates.draw("#statesvg", sampleData, tooltipHtml);
    	
    	d3.select(self.frameElement).style("height", "600px"); 
    </script>
 */

$(document).ready(function() {
	$('#preberiPredlogoBolnika').change(function() {
		$("#kreirajSporocilo").html("");
		var podatki = $(this).val().split(",");
		$("#kreirajIme").val(podatki[0]);
		$("#kreirajPriimek").val(podatki[1]);
		$("#kreirajVisino").val(podatki[2]);
		$("#kreirajTezo").val(podatki[3]);
	});
	
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});
	
});